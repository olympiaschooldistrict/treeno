package treeno

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
)

// TabInfoResponse response of getSavedTabs
type TabInfoResponse struct {
	TabInfoList  TabInfoList `json:"tabInfoList"`
	HasError     bool        `json:"hasError"`
	ErrorMessage string      `json:"errorMessage"`
}

// TabInfoList slice of tabs
type TabInfoList []struct {
	TabName    string `json:"tabName"`
	FileName   string `json:"fileName"`
	FileID     int    `json:"fileId"`
	DocID      int    `json:"docId"`
	FileSize   int    `json:"fileSize"`
	Department string `json:"department"`
	CabinetID  int    `json:"cabinetId"`
}

// TabRequest Request for list of tabs
type TabRequest struct {
	TabName    string `json:"tabName,"`
	FileName   string `json:"fileName,"`
	FileID     int    `json:"fileId,"`
	DocID      int    `json:"docId,"`
	FileSize   int    `json:"fileSize,"`
	Department string `json:"department,"`
	CabinetID  int    `json:"cabinetId,"`
}

// GetSavedTabs Gets list of saved tabs name for a cabinet
func (client *Client) GetSavedTabs(department string, cabinet string) (f TabInfoList, err error) {

	url := fmt.Sprintf("/departments/%s/cabinets/%s/savedTabs", department, cabinet)

	req, err := client.NewAuthRequest(url, "GET", nil)
	if err != nil {
		return
	}

	resp, err := client.c.Do(req)
	if err != nil {
		return
	}
	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("http error %v", resp.StatusCode)
	}

	res := TabInfoResponse{}

	bod, _ := ioutil.ReadAll(resp.Body)

	log.Println(string(bod))

	decoder := json.NewDecoder(bytes.NewReader(bod))
	err = decoder.Decode(&res)
	if err != nil {
		return nil, err
	}
	if res.HasError != false {
		return nil, fmt.Errorf("tab info  error  %v", res.ErrorMessage)
	}
	return res.TabInfoList, nil

}

// GetTabInfo UNTESTED, im not sure what this is for. I just noticed that it's a post not a get
// returns list of tabs for a folder in a cabinet.
func (client *Client) GetTabInfo(department string, cabinet string, folder string) (f TabInfoList, err error) {

	url := fmt.Sprintf("/tabs")

	cab, err := strconv.Atoi(cabinet)
	if err != nil {
		return nil, err
	}

	tab := TabRequest{
		TabName:    "",
		FileName:   "",
		FileID:     0,
		DocID:      0,
		FileSize:   0,
		Department: department,
		CabinetID:  cab,
	}

	jsonString, err := json.Marshal(tab)
	if err != nil {
		panic(err)
	}
	fmt.Println(jsonString)
	req, err := client.NewAuthRequest(url, "POST", bytes.NewBuffer(jsonString))
	if err != nil {
		return
	}
	fmt.Println(req)

	resp, err := client.c.Do(req)
	if err != nil {
		return
	}
	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("http error %v", resp.StatusCode)
	}

	res := TabInfoResponse{}

	bod, _ := ioutil.ReadAll(resp.Body)

	log.Println(string(bod))

	decoder := json.NewDecoder(bytes.NewReader(bod))
	err = decoder.Decode(&res)
	if err != nil {
		return nil, err
	}
	if res.HasError != false {
		return nil, fmt.Errorf("tab info  error  %v", res.ErrorMessage)
	}
	return res.TabInfoList, nil
}
