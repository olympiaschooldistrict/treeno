package treeno

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
)

// CabinetsResponse response containing any errors and the Cabinets
type CabinetsResponse struct {
	Cabinets     Cabinets `json:"cabinets"`
	HasError     bool     `json:"hasError"`
	ErrorMessage string   `json:"errorMessage"`
}

// Cabinets slice of cabinets for the department
type Cabinets []struct {
	IsAutoComplete        bool     `json:"isAutoComplete"`
	SavedTabs             []string `json:"savedTabs"`
	DataTypeDefinitions   struct{} `json:"dataTypeDefinitions"`
	Indices               []string `json:"indices"`
	DepartmentRealName    string   `json:"departmentRealName"`
	DepartmentDisplayName string   `json:"departmentDisplayName"`
	IsDocumentView        bool     `json:"isDocumentView"`
	CabinetID             int      `json:"cabinetId"`
	RealName              string   `json:"realName"`
	DisplayName           string   `json:"displayName"`
}

// CabinetSearchResponse contains results from a search, and errors if there was errors
type CabinetSearchResponse struct {
	Results      []CabinetSearchResult `json:"results"`
	HasError     bool                  `json:"hasError"`
	ErrorMessage string                `json:"errorMessage"`
}

type CabinetSearchFoldersResponse struct {
	CabinetSearchInfo CabinetSearchInfo `json:"CabinetSearchInfo"`
	HasError bool `json:"HasError"`
	ErrorMessage string                `json:"errorMessage"`
}

type CabinetSearchInfo struct {
	Department        string `json:"Department"`
	CabinetId         int    `json:"CabinetId"`
	ResultId          string `json:"ResultId"`
	ResultCount       int    `json:"ResultCount"`
	AuditStr          string `json:"AuditStr"`
	MatchingFolderIDs []int  `json:"MatchingFolderIDs"`
}

// CabinetSearchResult is the contents of a Cabinet Search
type CabinetSearchResult struct {
	DocId   int     `json:"docId"`
	Indices Indices `json:"cabinetIndices"`
}

// ListCabinets Returns a list of cabinets in a department
func (client *Client) ListCabinets() (cabinets Cabinets, err error) {

	url := fmt.Sprintf("/cabinets/%v", client.DefaultDepartment)

	req, err := client.NewAuthRequest(url, "GET", nil)
	if err != nil {
		return
	}

	resp, err := client.c.Do(req)
	if err != nil {
		return
	}
	if resp.StatusCode != http.StatusOK {
		return cabinets, fmt.Errorf("http error %v", resp.StatusCode)
	}

	cr := CabinetsResponse{}

	decoder := json.NewDecoder(resp.Body)
	err = decoder.Decode(&cr)
	if err != nil {
		return nil, err
	}
	if cr.HasError != false {
		return nil, fmt.Errorf("get cabinet error  %v", cr.ErrorMessage)
	}

	return cr.Cabinets, nil
}

// SearchCabinetFolders Searches cabinet for folders by index value. Returns only Folder IDs
// Gotcha: Cabinet index names are all lower case for some reason
func (client *Client) SearchCabinetFolders(cabinet string, query ...IndexQuery) (results CabinetSearchInfo, err error) {

	bod, err := json.Marshal(query)
	if err != nil {
		return results, err
	}

	path := fmt.Sprintf("/cabinets/search/%v/%v", client.DefaultDepartment, cabinet)

	req, err := client.NewAuthRequest(path, "POST", bytes.NewReader(bod))
	if err != nil {
		return
	}

	fmt.Println(string(bod))

	fmt.Printf("%+v", req)

	resp, err := client.c.Do(req)
	if err != nil {
		return
	}
	if resp.StatusCode != http.StatusOK {
		return results, fmt.Errorf("http error %v", resp.StatusCode)
	}
	sr := CabinetSearchFoldersResponse{}

	decoder := json.NewDecoder(resp.Body)
	err = decoder.Decode(&sr)
	if err != nil {
		return results, err
	}
	if sr.HasError != false {
		return results, fmt.Errorf("search cabinet error  %v", sr.ErrorMessage)
	}
	return sr.CabinetSearchInfo, nil
}

// SearchCabinet Searches cabinet for folders by index value
// Gotcha: Cabinet index names are all lower case for some reason
func (client *Client) SearchCabinet(cabinet string, query ...IndexQuery) (results []CabinetSearchResult, err error) {

	bod, err := json.Marshal(query)
	if err != nil {
		return nil, err
	}

	path := fmt.Sprintf("/cabinets/search/%v/%v/results", client.DefaultDepartment, cabinet)

	req, err := client.NewAuthRequest(path, "POST", bytes.NewReader(bod))
	if err != nil {
		return
	}

	resp, err := client.c.Do(req)
	if err != nil {
		return
	}
	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("http error %v", resp.StatusCode)
	}
	sr := CabinetSearchResponse{}

	decoder := json.NewDecoder(resp.Body)
	err = decoder.Decode(&sr)
	if err != nil {
		return nil, err
	}
	if sr.HasError != false {
		return nil, fmt.Errorf("get cabinet error  %v", sr.ErrorMessage)
	}
	return sr.Results, nil
}
