package treeno

import (
	"fmt"
	"strings"
	"time"
)

// DateFormat format used by treeno
const DateFormat = "01/02/2006"

// Date Valid Treeno date
type Date struct {
	time.Time
}

// MarshalJSON  Marshall to valid Treeno date
func (d *Date) MarshalJSON() ([]byte, error) {

	if d.Time.IsZero() {
		return []byte("\"\""), nil
	}
	return []byte("\"" + d.Format(DateFormat) + "\""), nil
}

// UnmarshalJSON Unmarshall from treeno time to go time
func (d *Date) UnmarshalJSON(data []byte) error {
	var t time.Time

	stripped := strings.TrimPrefix(strings.TrimSuffix(string(data), "\""), "\"")

	if len(stripped) == 0 {
		return nil
	}
	t, err := time.Parse(DateFormat, stripped)
	if err != nil {
		return err
	}

	d.Time = t
	return nil
}

// Bool Treeno representation of a bool, like "True", "Yes", etc.
type Bool bool

// MarshalJSON  Marshall to valid Treeno date
func (b *Bool) MarshalJSON() ([]byte, error) {

	if *b {
		return []byte("\"true\""), nil
	} else {
		return []byte("\"false\""), nil
	}
}

// UnmarshalJSON Unmarshall from treeno some kind of positive representation to go bool
func (b *Bool) UnmarshalJSON(data []byte) error {

	stripped := strings.TrimPrefix(strings.TrimSuffix(string(data), "\""), "\"")

	if strings.ToLower(stripped) == "true" {
		*b = true
		return nil
	} else if strings.ToLower(stripped) == "false" {
		*b = false
		return nil
	}

	if strings.ToLower(stripped) == "yes" {
		*b = true
		return nil
	} else if strings.ToLower(stripped) == "no" {
		*b = false
		return nil
	}

	if strings.ToLower(stripped) == "1" {
		*b = true
		return nil
	} else if strings.ToLower(stripped) == "0" {
		*b = false
		return nil
	}

	// if it's blank it's false, like an empty bool
	if stripped == "" {
		*b = false
		return nil
	}

	return fmt.Errorf("can't figure it out")

}
