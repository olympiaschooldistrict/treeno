package treeno

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"time"
)

// Client treeno client containing an http.Client that makes requests
type Client struct {
	c                 *http.Client
	Authentication    Authentication
	baseUrl           string
	DefaultDepartment string
}

// Do Make the request with the client
func (client Client) Do(req *http.Request) (*http.Response, error) {
	return client.c.Do(req)
}

// User login acting user
type User struct {
	ID       int    `json:"id"`
	Username string `json:"username"`
	Password string `json:"password"`
	LdapID   int    `json:"ldapId"`
	Email    string `json:"email"`
}

// Authentication authentication info
type Authentication struct {
	User         User       `json:"user"`
	Token        string     `json:"token"`
	RefreshToken string     `json:"refreshToken"`
	Expiration   treenoTime `json:"expiration"`
}

type treenoTime time.Time

const treenoTimeFmt = `"2006-01-02T15:04:05Z"`

// MarshalJSON implements the json.Marshaler interface.
func (t treenoTime) MarshalJSON() ([]byte, error) {
	b := time.Time(t).Format(treenoTimeFmt)
	return []byte(b), nil
}

// UnmarshalJSON implements the json.Unmarshaler interface.
func (t *treenoTime) UnmarshalJSON(data []byte) error {
	// Ignore null, like in the main JSON package.
	if string(data) == "null" {
		return nil
	}

	if string(data) == `"0001-01-01T00:00:00"` {
		return nil
	}

	// Fractional seconds are handled implicitly by Parse.
	var err error
	tt, err := time.Parse(treenoTimeFmt, string(data))
	if err != nil {
		return err
	}
	*t = treenoTime(tt)
	return err
}

// AuthResp authentication response
type AuthResp struct {
	Authentication Authentication `json:"authentication"`
	HasError       bool           `json:"hasError"`
	ErrorMessage   string         `json:"errorMessage"`
}

var ErrNotOK = fmt.Errorf("Status Not OK")

func ResponseBodyIfOK(resp *http.Response, err error) ([]byte, error) {
	if err != nil {
		return nil, err
	}
	respBodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("Unable to read body: %w", err)
	}

	if resp.StatusCode == http.StatusOK {
		return respBodyBytes, nil
	}
	return nil, fmt.Errorf("%w, resp: %q", ErrNotOK, string(respBodyBytes))
}
